#include <iostream>
#include <stdint.h>

void evenCount(int b, int i)
{
	 
	for (i ; i <= b; i += 2)
	{
		std::cout << i << " ";
	}
	std::cout << std::endl;
}

int main()
{		
	std::cout << "Enter number for even count: ";
	int num;
	std::cin >> num;
	evenCount(num, 0); //(number, 0 - even\ 1 - not even)

	return 0;
}
